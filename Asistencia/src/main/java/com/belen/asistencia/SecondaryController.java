package com.belen.asistencia;

import java.io.IOException;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class SecondaryController {
    @FXML
    private TableColumn<Metodos, Integer> DNI;
    @FXML
    private TableColumn<Metodos, String> nombre;
    @FXML
    private TableColumn<Metodos, CheckBox> entrada;
    @FXML
    private TableColumn<Metodos, CheckBox> salida;
    @FXML
    private TableView<Metodos> tabla;
    @FXML
    ObservableList<Metodos> list = FXCollections.observableArrayList();
    @FXML
    private void cargarDatos() throws IOException {
        for(int i = 0; i < 10; i++){
            CheckBox ckEntrada = new CheckBox("");
            CheckBox ckSalida = new CheckBox("");
            list.add(new Metodos(i,"Alex",ckEntrada,ckSalida));
            //getStylesheets().add(getClass().getResource("MyScrollPane.css").toExternalForm());
            //getStyleClass().add("red-border");
            //scene.getStylesheets().
        }
        tabla.setItems(list);
        DNI.setCellValueFactory(new PropertyValueFactory<Metodos, Integer>("DNI"));
        nombre.setCellValueFactory(new PropertyValueFactory<Metodos, String>("nombre"));
        entrada.setCellValueFactory(new PropertyValueFactory<Metodos, CheckBox>("entrada"));
        salida.setCellValueFactory(new PropertyValueFactory<Metodos, CheckBox>("salida"));
        
        //yeah
        /*
        
te paso un código de ejemplo que debes adaptar a tu problema pero que creo que te servirá de guía para resolver tu problema:

TableColumn<Object, Integer> columna = new TableColumn<>();
columna.setCellFactory(new Callback<TableColumn<Object,Integer>, TableCell<Object,Integer>>(){
        @Override
        public TableCell<Object, Integer> call(TableColumn<Object, Integer> tablecolumn) {
            return new TableCell<Object,Integer>(){
                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item != null){
                        if (item.intValue() > 5000){
                            setStyle("-fx-background-color: red;");
                        }
                        else if (item.intValue() == 200){
                            setStyle("-fx-background-color: green;");
                        }
                    }

                }
            };
        }
    });
        */
    }
    
    @FXML
    private void deleteProfesores() throws IOException{
        /*for(int i = 0; i < tabla.getItems().size(); i++){
            if(tabla.getItems().remove(tabla.getItems().get(i))){
                
            }
        }*/
        for(Metodos tb : tabla.getItems()){
            if(tb.getEntrada().isSelected()){
                Platform.runLater(() -> {
                    tabla.getItems().remove(tb);
                });
            }
        }
    }
    @FXML
    private void guardarCambios() throws IOException {
        //Acciones
    }
    @FXML
    private void switchToPrimary() throws IOException {
        App.setRoot("primary");
    }
}