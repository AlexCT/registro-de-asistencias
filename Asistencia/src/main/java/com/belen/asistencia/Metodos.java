/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.belen.asistencia;

import javafx.scene.control.CheckBox;

/**
 *
 * @author Alhaz
 */
public class Metodos {
    int DNI;
    String nombre;
    CheckBox entrada;
    CheckBox salida;

    public int getDNI() {
        return DNI;
    }

    public void setDNI(int DNI) {
        this.DNI = DNI;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public CheckBox getEntrada() {
        return entrada;
    }

    public void setEntrada(CheckBox entrada) {
        this.entrada = entrada;
    }

    public CheckBox getSalida() {
        return salida;
    }

    public void setSalida(CheckBox salida) {
        this.salida = salida;
    }

    public Metodos(int DNI, String nombre, CheckBox entrada, CheckBox salida) {
        this.DNI = DNI;
        this.nombre = nombre;
        this.entrada = entrada;
        this.salida = salida;
    }
    
}


