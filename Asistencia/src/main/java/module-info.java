module com.belen.asistencia {
    requires javafx.controls;
    requires javafx.fxml;

    opens com.belen.asistencia to javafx.fxml;
    exports com.belen.asistencia;
    requires mssql.jdbc;
}
